<?php

namespace Shop\WebshopBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Shop\WebShopBundle\Entity\WsCart;
use Shop\WebShopBundle\Entity\WsCartProduct;
use Symfony\Component\HttpFoundation\RedirectResponse;

class WsCartAdminController extends CRUDController
{
    public function clearCartAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if( count($object->getCartProduct()) > 0 ){
            $object->setActive(false);
            $em->persist($object);
            $em->flush();

            $cart = new WsCart();
            $cart->setIdUser($user);
            $cart->setActive(true);
            $cart->setRegDate(new \DateTime());
            $em->persist($cart);
            $em->flush();

            $this->addFlash('sonata_flash_success', 'Cart Cleared!');
        }else{
            $this->addFlash('sonata_flash_warning', 'No products added yet! Please add products to your cart.');
            return $this->redirect($this->generateUrl('admin_shop_webshop_wsproduct_list'));
        }
        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function restoreLastCartAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $dql = "SELECT C
                FROM ShopWebshopBundle:WsCart C
                WHERE C.idUser = :idUser
                AND C.active = false
                ORDER BY C.regDate DESC";

        $result = $em->createQuery($dql)
                    ->setParameters( array('idUser' => $user->getId()))
                    ->getResult();

        if ( count( $result ) > 0 ){
            $cart = $result[0];

            foreach ($cart->getCartProduct() as $cartProduct) {
                $cartProductNew = new WsCartProduct();
                $cartProductNew->setIdCart($object);
                $cartProductNew->setIdProduct($cartProduct->getIdProduct());
                $object->addCartProduct($cartProductNew);
            }

            $em->persist($object);
            $em->flush();

            $this->addFlash('sonata_flash_success', 'Last cart restored.');
        }else{
            $this->addFlash('sonata_flash_warning', 'No previous cart.');
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
