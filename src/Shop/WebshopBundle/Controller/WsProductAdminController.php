<?php

namespace Shop\WebshopBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Shop\WebShopBundle\Entity\WsCart;
use Shop\WebShopBundle\Entity\WsCartProduct;
use Symfony\Component\HttpFoundation\RedirectResponse;

class WsProductAdminController extends CRUDController
{
    public function addProductCartAction()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $cart = $em->getRepository('ShopWebshopBundle:WsCart')->findOneBy(array('idUser' => $user->getId(), 'active' => true));

        if(! $cart){
            $cart = new WsCart();
            $cart->setIdUser($user);
            $cart->setActive(true);
            $cart->setRegDate(new \DateTime());
            $em->persist($cart);
            $em->flush();
        }

        $cartProduct = new WsCartProduct();
        $cartProduct->setIdCart($cart);
        $cartProduct->setIdProduct($object);
        $cart->addCartProduct($cartProduct);

        $em->persist($cart);
        $em->flush();

        $this->addFlash('sonata_flash_success', 'Product Added To Cart!');

        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
