<?php

namespace Shop\WebshopBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Route\RouteCollection;

class WsCartAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('active')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('cartProduct', null, array('label' => 'Products in cart'))
            ->add('cartTotal', 'html', array('mapped' => false, 'property' => 'cartTotal'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'clearCart' => array('template' => 'ShopWebshopBundle:WsCart:list__action_clearcart.html.twig'),
                    'restoreCart' => array('template' => 'ShopWebshopBundle:WsCart:list__action_restorecart.html.twig')
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('cartProduct','sonata_type_collection',array('label' =>'Products in cart'),
                                                             array('edit' => 'inline', 'inline' => 'table'))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('cartProduct', 'sonata_type_collection', array('label'=>'Products in cart','route' => array('name' => 'show')),
                                                               array('edit' => 'inline','inline' => 'table'))
            ->add('cartSummary', 'html', array('mapped' => false, 'property' => 'cartSummary'))
        ;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser();

        return new ProxyQuery(
            $query
              ->where($query->getRootAlias() . '.idUser = ' . $currentUser->getId())
              ->andWhere($query->getRootAlias() . '.active = TRUE')
        );
    }

    public function prePersist($cart) {

        foreach ($cart->getCartProduct() as $cartProduct) {
            $cartProduct->setIdCart($cart);
        }
    }

    public function preUpdate($cart) {

        foreach ($cart->getCartProduct() as $cartProduct) {
            $cartProduct->setIdCart($cart);
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('clearCart', $this->getRouterIdParameter().'/clearCart');
        $collection->add('restoreLastCart', $this->getRouterIdParameter().'/restoreLastCart');
    }

}
