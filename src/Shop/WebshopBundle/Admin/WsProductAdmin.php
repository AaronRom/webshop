<?php

namespace Shop\WebshopBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class WsProductAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('author')
            ->add('idPublisher', null, array('label'=>'Publisher'))
            ->add('price')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('author')
            ->add('idPublisher', null, array('label'=>'Publisher'))
            ->add('price')
            ->add('productDiscount')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'addProductCart' => array('template' => 'ShopWebshopBundle:WsCart:list__action_addtocart.html.twig')
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Product Info')
                ->add('title')
                ->add('author')
                ->add('id_publisher', 'sonata_type_model', array('class' => 'ShopWebshopBundle:WsPublisher', 'label' => 'Publisher'))
                ->add('price', 'money', array('currency'=>'HUF'))
            ->end()
            ->with('Product Discounts')
                ->add('productDiscount','sonata_type_collection',array('label' =>'Discount'),
                                                                 array('edit' => 'inline', 'inline' => 'table'))
            ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Product Info')
                ->add('id')
                ->add('title')
                ->add('author')
                ->add('idPublisher', null, array('label'=>'Publisher'))
                ->add('price', 'money', array('currency'=>'HUF'))
            ->end()
            ->with('Product Discounts')
                ->add('productDiscount', 'sonata_type_collection', array('label'=>'Discount','route' => array('name' => 'show')),
                                                                   array('edit' => 'inline','inline' => 'table'))
            ->end()
        ;
    }

    public function prePersist($product) {

        foreach ($product->getProductDiscount() as $discount) {
            $discount->setIdProduct($product);
        }
    }

    public function preUpdate($product) {

        foreach ($product->getProductDiscount() as $discount) {
            $discount->setIdProduct($product);
        }
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('addProductCart', $this->getRouterIdParameter().'/addToCart');
    }
}
