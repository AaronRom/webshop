<?php

namespace Shop\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WsCartProduct
 *
 * @ORM\Table(name="ws_cart_product", indexes={@ORM\Index(name="IDX_A13FA84E808394B5", columns={"id_cart"}), @ORM\Index(name="IDX_A13FA84EDD7ADDD", columns={"id_product"})})
 * @ORM\Entity
 */
class WsCartProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ws_cart_product_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var \WsCart
     *
     * @ORM\ManyToOne(targetEntity="WsCart", inversedBy="cartProduct")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cart", referencedColumnName="id")
     * })
     */
    private $idCart;

    /**
     * @var \WsProduct
     *
     * @ORM\ManyToOne(targetEntity="WsProduct")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCart
     *
     * @param \Shop\WebshopBundle\Entity\WsCart $idCart
     * @return WsCartProduct
     */
    public function setIdCart(\Shop\WebshopBundle\Entity\WsCart $idCart = null)
    {
        $this->idCart = $idCart;

        return $this;
    }

    /**
     * Get idCart
     *
     * @return \Shop\WebshopBundle\Entity\WsCart
     */
    public function getIdCart()
    {
        return $this->idCart;
    }

    /**
     * Set idProduct
     *
     * @param \Shop\WebshopBundle\Entity\WsProduct $idProduct
     * @return WsCartProduct
     */
    public function setIdProduct(\Shop\WebshopBundle\Entity\WsProduct $idProduct = null)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return \Shop\WebshopBundle\Entity\WsProduct
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    public function __toString() {
        return $this->id ? $this->idProduct.'' : '';
    }
}
