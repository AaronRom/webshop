<?php

namespace Shop\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WsProductDiscount
 *
 * @ORM\Table(name="ws_product_discount", indexes={@ORM\Index(name="IDX_8168F0A8DD7ADDD", columns={"id_product"}), @ORM\Index(name="IDX_8168F0A8B13B589B", columns={"id_discount"})})
 * @ORM\Entity
 */
class WsProductDiscount
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ws_product_discount_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="discount_val", type="text", nullable=true)
     */
    private $discountVal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \WsProduct
     *
     * @ORM\ManyToOne(targetEntity="WsProduct", inversedBy="productDiscount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id")
     * })
     */
    private $idProduct;

    /**
     * @var \WsDiscount
     *
     * @ORM\ManyToOne(targetEntity="WsDiscount")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_discount", referencedColumnName="id")
     * })
     */
    private $idDiscount;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set discountVal
     *
     * @param string $discountVal
     * @return WsProductDiscount
     */
    public function setDiscountVal($discountVal)
    {
        $this->discountVal = $discountVal;

        return $this;
    }

    /**
     * Get discountVal
     *
     * @return string
     */
    public function getDiscountVal()
    {
        return $this->discountVal;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return WsProductDiscount
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set idProduct
     *
     * @param \Shop\WebshopBundle\Entity\WsProduct $idProduct
     * @return WsProductDiscount
     */
    public function setIdProduct(\Shop\WebshopBundle\Entity\WsProduct $idProduct = null)
    {
        $this->idProduct = $idProduct;

        return $this;
    }

    /**
     * Get idProduct
     *
     * @return \Shop\WebshopBundle\Entity\WsProduct
     */
    public function getIdProduct()
    {
        return $this->idProduct;
    }

    /**
     * Set idDiscount
     *
     * @param \Shop\WebshopBundle\Entity\WsDiscount $idDiscount
     * @return WsProductDiscount
     */
    public function setIdDiscount(\Shop\WebshopBundle\Entity\WsDiscount $idDiscount = null)
    {
        $this->idDiscount = $idDiscount;

        return $this;
    }

    /**
     * Get idDiscount
     *
     * @return \Shop\WebshopBundle\Entity\WsDiscount
     */
    public function getIdDiscount()
    {
        return $this->idDiscount;
    }

    public function __toString() {
        return $this->id ? $this->discountVal.' '.$this->idDiscount.' discount' : '';
    }
}
