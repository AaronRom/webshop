<?php

namespace Shop\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WsProduct
 *
 * @ORM\Table(name="ws_product", indexes={@ORM\Index(name="IDX_FC11333E1CDD6961", columns={"id_publisher"})})
 * @ORM\Entity
 */
class WsProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ws_product_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="text", nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var \WsPublisher
     *
     * @ORM\ManyToOne(targetEntity="WsPublisher")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_publisher", referencedColumnName="id")
     * })
     */
    private $idPublisher;

    /**
     *
     * @ORM\OneToMany(targetEntity="WsProductDiscount", mappedBy="idProduct", cascade={"all"}, orphanRemoval=true)
     *
     */
    private $productDiscount;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return WsProduct
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return WsProduct
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return WsProduct
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set idPublisher
     *
     * @param \Shop\WebshopBundle\Entity\WsPublisher $idPublisher
     * @return WsProduct
     */
    public function setIdPublisher(\Shop\WebshopBundle\Entity\WsPublisher $idPublisher = null)
    {
        $this->idPublisher = $idPublisher;

        return $this;
    }

    /**
     * Get idPublisher
     *
     * @return \Shop\WebshopBundle\Entity\WsPublisher
     */
    public function getIdPublisher()
    {
        return $this->idPublisher;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productDiscount = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productDiscount
     *
     * @param \Shop\WebshopBundle\Entity\WsProductDiscount $productDiscount
     * @return WsProduct
     */
    public function addProductDiscount(\Shop\WebshopBundle\Entity\WsProductDiscount $productDiscount)
    {
        $this->productDiscount[] = $productDiscount;

        return $this;
    }

    /**
     * Remove productDiscount
     *
     * @param \Shop\WebshopBundle\Entity\WsProductDiscount $productDiscount
     */
    public function removeProductDiscount(\Shop\WebshopBundle\Entity\WsProductDiscount $productDiscount)
    {
        $this->productDiscount->removeElement($productDiscount);
    }

    /**
     * Get productDiscount
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductDiscount()
    {
        return $this->productDiscount;
    }

    public function __toString() {
        return $this->id ? $this->title : '';
    }

}
