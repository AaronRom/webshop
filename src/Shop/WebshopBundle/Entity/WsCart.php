<?php

namespace Shop\WebshopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WsCart
 *
 * @ORM\Table(name="ws_cart", indexes={@ORM\Index(name="IDX_A9B141056B3CA4B", columns={"id_user"})})
 * @ORM\Entity
 */
class WsCart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="ws_cart_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reg_date", type="datetime", nullable=false)
     */
    private $regDate;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     *
     * @ORM\OneToMany(targetEntity="WsCartProduct", mappedBy="idCart", cascade={"all"}, orphanRemoval=true)
     *
     */
    private $cartProduct;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return WsCart
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set regDate
     *
     * @param \DateTime $regDate
     * @return WsCart
     */
    public function setRegDate($regDate)
    {
        $this->regDate = $regDate;

        return $this;
    }

    /**
     * Get regDate
     *
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->regDate;
    }

    /**
     * Set idUser
     *
     * @param \Application\Sonata\UserBundle\Entity\User $idUser
     * @return WsCart
     */
    public function setIdUser(\Application\Sonata\UserBundle\Entity\User $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cartProduct = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cartProduct
     *
     * @param \Shop\WebshopBundle\Entity\WsCartProduct $cartProduct
     * @return WsCart
     */
    public function addCartProduct(\Shop\WebshopBundle\Entity\WsCartProduct $cartProduct)
    {
        $this->cartProduct[] = $cartProduct;

        return $this;
    }

    /**
     * Remove cartProduct
     *
     * @param \Shop\WebshopBundle\Entity\WsCartProduct $cartProduct
     */
    public function removeCartProduct(\Shop\WebshopBundle\Entity\WsCartProduct $cartProduct)
    {
        $this->cartProduct->removeElement($cartProduct);
    }

    /**
     * Get cartProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartProduct()
    {
        return $this->cartProduct;
    }

    public function cartSummary(){
        $summary = '';
        $subtotal = 0;
        $totalDiscount = 0;
        $total = 0;
        $lower = null;
        $publisherProducts = array();
        $publisherProductsLower = array();
        $publisherProductsApply = array();

        foreach ($this->cartProduct as $cProduct) {
            $product = $cProduct->getIdProduct();
            if (!isset($publisherProductsLower[$product->getIdPublisher()->getName()])){
                $publisherProductsLower[$product->getIdPublisher()->getName()] = $product->getPrice();
            }
            else{
                if($product->getPrice() < $publisherProductsLower[$product->getIdPublisher()->getName()]){
                    $publisherProductsLower[$product->getIdPublisher()->getName()] = $product->getPrice();
                }
            }
            $publisherProducts[$product->getIdPublisher()->getName()] = (isset($publisherProducts[$product->getIdPublisher()->getName()]) ? $publisherProducts[$product->getIdPublisher()->getName()] : 0 ) + 1;
        }

        foreach ($this->cartProduct as $cProduct) {
            $product = $cProduct->getIdProduct();
            $subtotal += $product->getPrice();
            foreach ($product->getProductDiscount() as $pDiscount) {
                $discount = $pDiscount->getIdDiscount();
                if( $discount->getId() == 101 ){
                        $totalDiscount = $totalDiscount + ( $product->getPrice() * ( intval($pDiscount->getDiscountVal()) / 100 ) );
                }elseif( $discount->getId() == 102 ){
                    $totalDiscount = $totalDiscount + ( intval($pDiscount->getDiscountVal()) );
                }elseif( $discount->getId() == 103 ){
                    if( $publisherProducts[$product->getIdPublisher()->getName()] > 2 ){
                        if( ! isset($publisherProductsApply[$product->getIdPublisher()->getName()])){
                            $totalDiscount = $totalDiscount + $publisherProductsLower[$product->getIdPublisher()->getName()];
                            $publisherProductsApply[$product->getIdPublisher()->getName()] = true;
                        }
                    }
                }
            }
        }
        $total = $subtotal - $totalDiscount;
        $summary = $summary.'Subtotal: '.$subtotal.'<br/>Discount: '.$totalDiscount.'<br/>Total: '.$total;

        return $summary;
    }

    public function cartTotal(){
        $subtotal = 0;
        $totalDiscount = 0;
        $total = 0;
        $lower = null;
        $publisherProducts = array();
        $publisherProductsLower = array();
        $publisherProductsApply = array();

        foreach ($this->cartProduct as $cProduct) {
            $product = $cProduct->getIdProduct();
            if (!isset($publisherProductsLower[$product->getIdPublisher()->getName()])){
                $publisherProductsLower[$product->getIdPublisher()->getName()] = $product->getPrice();
            }
            else{
                if($product->getPrice() < $publisherProductsLower[$product->getIdPublisher()->getName()]){
                    $publisherProductsLower[$product->getIdPublisher()->getName()] = $product->getPrice();
                }
            }
            $publisherProducts[$product->getIdPublisher()->getName()] = (isset($publisherProducts[$product->getIdPublisher()->getName()]) ? $publisherProducts[$product->getIdPublisher()->getName()] : 0 ) + 1;
        }

        foreach ($this->cartProduct as $cProduct) {
            $product = $cProduct->getIdProduct();
            $subtotal += $product->getPrice();
            foreach ($product->getProductDiscount() as $pDiscount) {
                $discount = $pDiscount->getIdDiscount();
                if( $discount->getId() == 101 ){
                        $totalDiscount = $totalDiscount + ( $product->getPrice() * ( intval($pDiscount->getDiscountVal()) / 100 ) );
                }elseif( $discount->getId() == 102 ){
                    $totalDiscount = $totalDiscount + ( intval($pDiscount->getDiscountVal()) );
                }elseif( $discount->getId() == 103 ){
                    if( $publisherProducts[$product->getIdPublisher()->getName()] > 2 ){
                        if( ! isset($publisherProductsApply[$product->getIdPublisher()->getName()])){
                            $totalDiscount = $totalDiscount + $publisherProductsLower[$product->getIdPublisher()->getName()];
                            $publisherProductsApply[$product->getIdPublisher()->getName()] = true;
                        }
                    }
                }
            }
        }
        $total = $subtotal - $totalDiscount;

        return $total;
    }

    public function __toString() {
        return $this->id ? 'My Cart ('.count($this->cartProduct).' Products)' : '';
    }
}
